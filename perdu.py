# -*- coding: utf-8 -*-
import pyglet
import math
import json

# initialisation des variables
window = pyglet.window.Window(700, 700)

vaisseau = pyglet.image.load('vaisseau_millenium.jpg')
vaisseau.anchor_x = vaisseau.width // 2
vaisseau.anchor_y = vaisseau.height // 2
vaisseauSprite = pyglet.sprite.Sprite(vaisseau)
imagepositionfinale = pyglet.image.load('posfinpyth.png')


up = False
right = False
left = False

speedx = 0
speedy = 0
counter = .0

monde = open('monde2.json')
coord = json.load(monde)
position_dep = coord[0]
position_fin = coord[1]
obstacles = coord[2]
vitesse_avant = coord[3]
energie = coord[4]
vitesse_ang = coord[5]
elasti = coord[6]
vaisseauSprite.x = position_dep[0]
vaisseauSprite.y = position_dep[1]


@window.event()
def on_draw():
    window.clear()
    Obstacle().draw()
    vaisseauSprite.draw()
    imagepositionfinale.blit(position_fin[0], position_fin[1])


@window.event()
def on_key_press(symbol, handler):
    global up
    global left
    global right
    key = pyglet.window.key
    if symbol == key.LEFT:
        left = True
    if symbol == key.RIGHT:
        right = True
    if symbol == key.UP:
        up = True


@window.event()
def on_key_release(symbol, handler):
    global up
    global left
    global right
    key = pyglet.window.key
    if symbol == key.LEFT:
        left = False
    if symbol == key.RIGHT:
        right = False
    if symbol == key.UP:
        up = False


class Vaisseau:
    def __init__(self):
        self.left = left
        self.right = right
        self.up = up
        self.acceleration = 0
        self.vaisseau = vaisseauSprite
        self.orientation = -self.vaisseau.rotation % 360
        self.accelx = 0
        self.accely = 0
        self.speedx = speedx
        self.speedy = speedy
        self.posx = self.vaisseau.x
        self.posy = self.vaisseau.y
        self.angle = math.atan2(self.speedx, self.speedy)
        self.position = (self.posx, self.posy)
        self.vitesse = (self.speedx, self.speedy)
        self.elasti = elasti

    def movement(self):
        global speedx
        global speedy
        if self.left is True:
            self.vaisseau.rotation -= 6
        if self.right is True:
            self.vaisseau.rotation += 6
        if self.up is True:
            self.acceleration += 0.1
        if self.speedx <= -4:
            speedx = -4
        if self.speedx >= 4:
            speedx = 4
        if self.speedy <= -4:
            speedy = -4
        if self.speedy >= 4:
            speedy = 4
        self.accelx = self.acceleration * math.cos(self.orientation * math.pi / 180)
        self.accely = self.acceleration * math.sin(self.orientation * math.pi / 180)
        speedx += self.accelx
        speedy += self.accely
        self.vaisseau.x += self.speedx
        self.vaisseau.y += self.speedy

    def boundaries(self):
        global speedx
        global speedy
        if vaisseauSprite.x >= 700:
            vaisseauSprite.x = 700
            speedy = -speedy * -math.sin(self.angle)
            speedx = -speedx * self.elasti
        if vaisseauSprite.x <= 0:
            vaisseauSprite.x = 0
            speedy = -speedy * math.sin(self.angle)
            speedx = -speedx * self.elasti
        if vaisseauSprite.y >= 700:
            vaisseauSprite.y = 700
            speedy = -speedy * self.elasti
            speedx = -speedx * -math.cos(self.angle)
        if vaisseauSprite.y <= 0:
            vaisseauSprite.y = 0
            speedy = -speedy * self.elasti
            speedx = -speedx * math.cos(self.angle)


class Obstacle(Vaisseau):
    def __init__(self):
        super(Obstacle, self).__init__()
        self.obstacles = map(lambda o: o + [o[0]], obstacles)

    def draw(self):
        gl = pyglet.gl
        gl.glClear(gl.GL_COLOR_BUFFER_BIT)
        gl.glLoadIdentity()
        for poly in self.obstacles:
            gl.glBegin(gl.GL_LINES)
            for pt, pt2 in zip(poly, poly[1:]):
                gl.glVertex2f(*pt)
                gl.glVertex2f(*pt2)
            gl.glVertex2f(*poly[-1])
            gl.glVertex2f(*poly[0])
            gl.glEnd()

    def collisions(self):
        for poly in self.obstacles:
            for pt1, pt2 in zip(poly, poly[1:]):
                pt1 = tuple(pt1)
                pt2 = tuple(pt2)

                delta_q = (pt2[0] - pt1[0], pt2[1] - pt1[1])
                diffqp = (pt1[0] - vaisseauSprite.x, pt1[1] - vaisseauSprite.y)
                r1 = ((diffqp[0] * delta_q[1]) - (diffqp[1] * delta_q[0]))
                dpxdq = ((speedx * delta_q[1]) - (speedy * delta_q[0]))
                s1 = ((diffqp[0] * speedy) - (diffqp[1] * speedx))

                if r1 != 0 and dpxdq == 0:
                    pass

                elif dpxdq != 0:
                    r = r1 / dpxdq
                    s = s1 / dpxdq
                    if 0 < s <= 1 and 0 < r <= 1:
                        self.react_collide(delta_q)

    def react_collide(self, delta_q):
        global speedx
        global speedy
        modulen = math.sqrt((-delta_q[1])**2 + (delta_q[0])**2)
        normale = (-delta_q[1] / modulen, delta_q[0] / modulen)
        pscale = speedx * normale[0] + speedy * normale[1]
        pprime0 = speedx - 2 * pscale * normale[0]
        pprime1 = speedy - 2 * pscale * normale[1]
        modulev = math.sqrt(speedx**2 + speedy**2)
        modulepprime = math.sqrt(pprime0**2 + pprime1**2)
        divmodule = modulev / modulepprime
        speedx = elasti * pprime0 * divmodule
        speedy = elasti * pprime1 * divmodule


@window.event()
def update_frames(dt):
    global counter
    termine = False
    while not termine:
        counter += dt
        v = Vaisseau()
        o = Obstacle()
        v.movement()
        v.boundaries()
        o.collisions()
        if position_fin[0] - 20 < vaisseauSprite.x < position_fin[0] + 20 and position_fin[1] - 20 < vaisseauSprite.y < position_fin[1] + 20:
            exit()
        return termine


pyglet.clock.schedule_interval(update_frames, 1 / 120.0)
pyglet.app.run()
